pullOtpOps <- function(route, startDate, endDate){
  # inputs: startDate (character): yyyy-mm-dd
  #         endDate (character): yyyy-mm-dd
  # output: individual time point crossing with a 1/0 indicator whether it was early, late, or on-time
  
  library(DBI)
  library(data.table)
  start <- as.integer(gsub('-', '', startDate))
  end <- as.integer(gsub('-', '', endDate))
  # endDate <- 20170401
  otpQuery <- paste0("
                     set transaction isolation level read uncommitted; 
                     SELECT
                     fact_avl.avlsk,
                     fact_avl.trip_tmsk,
                     fact_avl.stopsk,
                     fact_avl.Date_Key,
                     fact_avl.operatorsk,
                     fact_avl.vehiclesk,
                     DIM_TRIP_TM.line_type,
                     DIM_TRIP_TM.block_provider,
                     DIM_DATE.Month_Of_Year,
                     DIM_DATE.Calendar_Year_Month,
                     dim_date.full_date,
                     DIM_VEHICLE.veh_id,
                     block_number,
                     ltrim(rtrim(DIM_TRIP_TM.line_id)) as 'line_id',
                     ltrim(rtrim(DIM_TRIP_TM.line_direction)) as 'line_direction',
                     trip_number,
                     allstop_sequence_number,
                     node_id,
                     dim_stop.site_id,
                     sched_tp_prevt,
                     dep_tp_prevt,
                     sched_1st_tp_thist,
                     FACT_AVL.sched_time - ( FACT_AVL.sched_tp_prevt + FACT_AVL.sched_1st_tp_thist) as scheduledHeadway,
                     FACT_AVL.act_dep_at_tp - (FACT_AVL.dep_tp_prevt + FACT_AVL.sched_1st_tp_thist) as actualHeadway,
                     this_1st_tp,
                     this_2nd_tp,
                     this_last_tp,
                     is_layover,
                     sched_time,
                     adherence,
                     act_dep_at_tp,
                     early_allowed_flag,
                     late_allowed_flag,
                     revenue_id,
                     case 
                     when FACT_AVL.REVENUE_ID = 'D' then null
                     when FACT_AVL.ACT_DEP_AT_TP is null then null
                     when FACT_AVL.ADHERENCE is null then null
                     when FACT_AVL.ADHERENCE >= 1800 then null
                     when FACT_AVL.ADHERENCE <= -1800 then null
                     else
                     case 
                     when FACT_AVL.IS_LAYOVER = 1 then 0
                     when FACT_AVL.EARLY_ALLOWED_FLAG = 1 then 0
                     when FACT_AVL.ADHERENCE > 59 then 1
                     else 0
                     end
                     end as 'early',
                     case 
                     when FACT_AVL.REVENUE_ID = 'D' then null
                     when FACT_AVL.ACT_DEP_AT_TP is null then null
                     when FACT_AVL.ADHERENCE is null then null
                     when FACT_AVL.ADHERENCE >= 1800 then null
                     when FACT_AVL.ADHERENCE <= -1800 then null
                     else
                     case
                     when FACT_AVL.IS_LAYOVER = 1 then 0
                     when FACT_AVL.LATE_ALLOWED_FLAG = 1 then 0
                     when FACT_AVL.ADHERENCE < -299 then 1
                     else 0
                     end
                     end as 'late',
                     case 
                     when FACT_AVL.REVENUE_ID = 'D' then null
                     when FACT_AVL.ACT_DEP_AT_TP is null then null
                     when FACT_AVL.ADHERENCE is null then null
                     when FACT_AVL.ADHERENCE >= 1800 then null
                     when FACT_AVL.ADHERENCE <= -1800 then null
                     else
                     (1 
                     - case 
                     when FACT_AVL.IS_LAYOVER = 1 then 0
                     when FACT_AVL.EARLY_ALLOWED_FLAG = 1 then 0
                     when FACT_AVL.ADHERENCE > 59 then 1
                     else 0
                     end
                     - case 
                     when FACT_AVL.IS_LAYOVER = 1 then 0
                     when FACT_AVL.LATE_ALLOWED_FLAG = 1 then 0
                     when FACT_AVL.ADHERENCE < -299 then 1
                     else 0
                     end)
                     end as 'on_time',
                     case 
                     when FACT_AVL.REVENUE_ID = 'D' then null
                     when FACT_AVL.ACT_DEP_AT_TP is null then null
                     when FACT_AVL.ADHERENCE is null then null
                     when FACT_AVL.ADHERENCE >= 1800 then null
                     when FACT_AVL.ADHERENCE <= -1800 then null
                     else
                     case 
                     when FACT_AVL.IS_LAYOVER = 1 then 0
                     when FACT_AVL.EARLY_ALLOWED_FLAG = 1 then 0
                     when FACT_AVL.ADHERENCE > 59 then 1
                     else 0
                     end
                     end 
                     + case 
                     when FACT_AVL.REVENUE_ID = 'D' then null
                     when FACT_AVL.ACT_DEP_AT_TP is null then null
                     when FACT_AVL.ADHERENCE is null then null
                     when FACT_AVL.ADHERENCE >= 1800 then null
                     when FACT_AVL.ADHERENCE <= -1800 then null
                     else
                     case
                     when FACT_AVL.IS_LAYOVER = 1 then 0
                     when FACT_AVL.LATE_ALLOWED_FLAG = 1 then 0
                     when FACT_AVL.ADHERENCE < -299 then 1
                     else 0
                     end
                     end 
                     + case 
                     when FACT_AVL.REVENUE_ID = 'D' then null
                     when FACT_AVL.ACT_DEP_AT_TP is null then null
                     when FACT_AVL.ADHERENCE is null then null
                     when FACT_AVL.ADHERENCE >= 1800 then null
                     when FACT_AVL.ADHERENCE <= -1800 then null
                     else
                     (1
                     - case
                     when FACT_AVL.IS_LAYOVER = 1 then 0
                     when FACT_AVL.EARLY_ALLOWED_FLAG = 1 then 0
                     when FACT_AVL.ADHERENCE > 59 then 1
                     else 0
                     end
                     - case 
                     when FACT_AVL.IS_LAYOVER = 1 then 0
                     when FACT_AVL.LATE_ALLOWED_FLAG = 1 then 0
                     when FACT_AVL.ADHERENCE < -299 then 1
                     else 0
                     end)
                     end as 'total_crossings',
                     DIM_TRIP_TM.service_id,
                     case DIM_TRIP_TM.service_id
                     when 'WK' then 'Weekday'
                     when 'SAT' then 'Saturday'
                     when 'SUN' then 'Sunday'
                     when 'RED' then 'Reduced'
                     when 'HOL' then 'Holiday'
                     when '   ' then 'Unknown'
                     else service_id
                     end as 'service_id',
                     DIM_DATE.Week_Of_Year,
                     DIM_DATE.full_date,
                     DIM_DATE.Day_Name_Of_Week,
                     DATEADD(dd, -1 * (Day_Of_Week - 1), DIM_DATE.Full_Date),
                     CASE 
                     WHEN DIM_TRIP_TM.LINE_TYPE = 'Express' THEN 'Express'
                     WHEN DIM_TRIP_TM.LINE_TYPE like '%Loc%' THEN 'Local'
                     ELSE 'Unknown'
                     END as 'line_type',
                     case 
                     when DIM_BAD_DAY_TRIP_ALL_VIEW.date_key is null then 'Good Days'
                     else 'Bad Days' 
                     end as 'day_type',
                     dim_stop.site_id,
                     dim_stop.site_on,
                     dim_stop.site_at,
                     dim_stop.site_downtown_zone,
                     dim_stop.city_id,
                     allstop_sequence_number,
                     site_latitude,
                     site_longitude,
                     avl_segment_name,
                     avl_segment,
                     node_id,
                     node_name,
                     stop_sequence_number,
                     pattern_stop_sequence,
                     terminal_letter,
                     trip_sequence_number,
                     pat_id,
                     time_datetime,
                     time_of_day,
                     oper_id,
                     veh_id,
                     CONCAT(Full_date, ' ', hour_of_day_24, ':', minute_of_hour, ':', second_of_minute),
                     line_pay_leave
                     FROM
                     DIM_BAD_DAY_TRIP_ALL_VIEW RIGHT OUTER JOIN FACT_AVL ON (DIM_BAD_DAY_TRIP_ALL_VIEW.trip_tmsk = FACT_AVL.trip_tmsk and DIM_BAD_DAY_TRIP_ALL_VIEW.DATE_KEY = FACT_AVL.Date_Key and DIM_BAD_DAY_TRIP_ALL_VIEW.fact_type = 'AVL'
                     )
                     INNER JOIN DIM_TRIP_TM ON (DIM_TRIP_TM.TRIP_TMSK=FACT_AVL.trip_tmsk)
                     INNER JOIN DIM_TIME ON (dim_time.TIME_KEY=fact_avl.SCHED_TIME)
                     INNER JOIN DIM_DATE ON (FACT_AVL.date_key=DIM_DATE.Date_Key)
                     INNER JOIN DIM_STOP on FACT_AVL.stopsk=DIM_STOP.stopsk
                     INNER JOIN DIM_OPERATOR ON FACT_AVL.operatorsk = dim_operator.operatorsk
                     INNER JOIN DIM_VEHICLE on fact_avl.vehiclesk = dim_vehicle.vehiclesk
                     WHERE
                     DIM_STOP.site_downtown_zone = 'Y'
                     AND DIM_TRIP_TM.BLOCK_PROVIDER= 'Metro Transit' 
                     AND DIM_TRIP_TM.line_id = '", route, "'
                     and DIM_TRIP_TM.trip_number <> -1
                     AND DIM_DATE.date_key >= '", start, "'
                     AND DIM_DATE.date_key < '", end, "'")
  channel <- dbConnect(odbc::odbc(), "db_SDWarehouse")
  filename <- paste0(route, "otp_", startDate, "_", endDate, ".RDS")
  avl <- data.table(dbGetQuery(channel, otpQuery))
  saveRDS(avl, file = filename)
  return(avl)
}





plot_otp <- function(dat){
  library(ggplot2)
  library(MetroTransitr)
  dat[, range(full_date)]
  dat[, .N, site_downtown_zone]
  
  datemlt <- melt(dat[, .(early, late, on_time, full_date, site_downtown_zone, line_id)], id.vars = c('full_date', 'site_downtown_zone', 'line_id'), variable.name = 'OTP', value.name = 'value')
  dateotp <- datemlt[, sum(value, na.rm = T), keyby = .(full_date, site_downtown_zone, line_id, OTP)]
  datesum <- dateotp[, .(trips = sum(V1)), .(full_date, site_downtown_zone, line_id)]
  dateotp <- datesum[dateotp, on = .(full_date, site_downtown_zone, line_id)]
  dateotp[, frac_otp := (V1 / trips)]
  
  # ggplot(dateotp[site_downtown_zone == 'Y'], aes(x = full_date, y = frac_otp, col = OTP, lty = site_downtown_zone)) + 
  #   theme_bw(base_size = 15) + 
  #   # geom_line() + 
  #   geom_smooth(se = F, size = 0.5) + 
  #   scale_color_manual(values = MT_palette[c(3, 2, 1)]) + 
  #   ylab('fraction of trips')
  
  # detour annotation
  # The original detour off of Nicollet Mall occurred with the August 2015 pick (August 22, 2015). The detour was amended midpick (November 7) to move to Hennepin. Schedules did not catch up to the revised detour until March 19, 2016. If you’re thinking that must have been a nightmare, I can confirm that it was!
  # hennepin detour started 
  nicmall <- as.Date(c("2015-08-22", "2015-11-07", "2016-03-19", "2019-04-15", "2019-06-08"))
  
  p <- ggplot(dateotp[site_downtown_zone == 'Y'], aes(x = as.Date(full_date), y = frac_otp, fill = OTP)) + 
    theme_minimal() + 
    geom_bar(position = 'fill', stat = 'identity') + 
    # geom_smooth(se = F, size = 0.5) +
    ylab('fraction of trips') + 
    scale_x_date(name = '') +
    geom_vline(xintercept = nicmall, lty = 3, color = 'white') + 
    ggtitle(paste0("Route ", dateotp[, paste(unique(line_id), collapse = ',')], " OTP in downtown zones")) +
    theme(plot.title = element_text(hjust = 0.5)) +
    scale_fill_manual(name = 'status', labels = c('early', 'late', 'on time'), values = MT_palette[c(3, 2, 1)])
  
  filename = paste0('route-', dateotp[, paste(unique(line_id), collapse = ',')], '-otp-downtown-2015-2019.pdf')
  ggsave(p, file = filename, width = 10, height = 8)
  print(filename)
  system(paste0('open ' ,  filename))          
}



plot_otp19 <- function(dat){
  library(ggplot2)
  library(MetroTransitr)
  dat <- dat[full_date > as.POSIXct('2018-12-31')]
  dat[, range(full_date)]
  dat[, .N, site_downtown_zone]
  
  datemlt <- melt(dat[, .(early, late, on_time, full_date, site_downtown_zone, line_id)], id.vars = c('full_date', 'site_downtown_zone', 'line_id'), variable.name = 'OTP', value.name = 'value')
  dateotp <- datemlt[, sum(value, na.rm = T), keyby = .(full_date, site_downtown_zone, line_id, OTP)]
  datesum <- dateotp[, .(trips = sum(V1)), .(full_date, site_downtown_zone, line_id)]
  dateotp <- datesum[dateotp, on = .(full_date, site_downtown_zone, line_id)]
  dateotp[, frac_otp := (V1 / trips)]
  
  # ggplot(dateotp[site_downtown_zone == 'Y'], aes(x = full_date, y = frac_otp, col = OTP, lty = site_downtown_zone)) + 
  #   theme_bw(base_size = 15) + 
  #   # geom_line() + 
  #   geom_smooth(se = F, size = 0.5) + 
  #   scale_color_manual(values = MT_palette[c(3, 2, 1)]) + 
  #   ylab('fraction of trips')
  
  # detour annotation
  # The original detour off of Nicollet Mall occurred with the August 2015 pick (August 22, 2015). The detour was amended midpick (November 7) to move to Hennepin. Schedules did not catch up to the revised detour until March 19, 2016. If you’re thinking that must have been a nightmare, I can confirm that it was!
  # hennepin detour started 
  nicmall <- as.Date(c("2015-08-22", "2015-11-07", "2016-03-19", "2019-04-15", "2019-06-08"))
  
  p <- ggplot(dateotp[site_downtown_zone == 'Y'], aes(x = as.Date(full_date), y = frac_otp, fill = OTP)) + 
    theme_minimal() + 
    geom_bar(position = 'fill', stat = 'identity') + 
    # geom_smooth(se = F, size = 0.5) +
    ylab('fraction of trips') + 
    scale_x_date(name = '') +
    geom_vline(xintercept = nicmall, lty = 3, color = 'white') + 
    ggtitle(paste0("Route ", dateotp[, paste(unique(line_id), collapse = ',')], " OTP in downtown zones")) +
    theme(plot.title = element_text(hjust = 0.5)) +
    scale_fill_manual(name = 'status', labels = c('early', 'late', 'on time'), values = MT_palette[c(3, 2, 1)])
  
  filename = paste0('route-', dateotp[, paste(unique(line_id), collapse = ',')], '-otp-downtown-2019.pdf')
  ggsave(p, file = filename, width = 10, height = 8)
  print(filename)
  system(paste0('open ' ,  filename))          
}


routes <- c(5, 9, 14, 19, 22, 4, 6, 10, 11, 12, 17, 18, 25, 61)

for(i in routes){
  rdat <- pullOtpOps(route = i, startDate = 20150101, endDate = 20190630)
  plot_otp(rdat)
}


for(i in routes){
  rdat <- readRDS(paste0(i, 'otp_20150101_20190630.RDS'))
  plot_otp19(rdat)
}


