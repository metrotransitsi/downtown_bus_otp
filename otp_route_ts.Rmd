---
title: "On-time performance time series by route"
author: "Raven McKnight"
date: "7/22/2019"
output:
  MetroTransitr::metro_html: default
  MetroTransitr::metro_presentation: default
  MetroTransitr::metro_report: default
  self contained: no
---

```{r, echo = FALSE, out.width="100%"}
library(knitr)
myimages<-list.files("vis/", pattern = ".pdf")
include_graphics(myimages)
```